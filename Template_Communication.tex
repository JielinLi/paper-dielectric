\documentclass[journal,twoside,shortpaper,9pt]{IEEEtran}
\usepackage{textcomp}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{multicol,multirow}
\usepackage{array}
\usepackage{romannum}
\usepackage{float}
\usepackage[caption = false]{subfig}
\usepackage{float}
\usepackage[normalem]{ulem}
\usepackage{cancel}
\usepackage{soul,xcolor}

%% --------------------- IEEE Macros ---------------------------------

\hyphenation{Section Table}

\newcolumntype{L}{>{\raggedright\arraybackslash}p}
\newcolumntype{C}{>{\centering\arraybackslash}m}

\def\argmin{\mathop{\rm arg\,min\,}}

\newtheorem{theorem}{Theorem}
\newtheorem{assumption}{Assumption}
\newtheorem{remark}{Remark}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{corollary}{Corollary}
\newtheorem{example}{Example}
\newtheorem{proposition}{Proposition}

\markboth{IEEE TRANSACTIONS ON ANTENNAS AND PROPAGATION}{IEEE TRANSACTIONS ON ANTENNAS AND PROPAGATION}

\begin{document}

\title{The Impact of Integral Accuracy on the Stability of Time Domain Integral Equations for Scattering from Dielectrics}
\author{Jielin Li, \textit{Student Member, IEEE} and Daniel S. Weile, \textit{Fellow, IEEE}
\thanks{Manuscript received April 20, 2018.}
\thanks{Jielin Li and Daniel S. Weile are with the Department of Electrical and Computer Engineering, University of Delaware, Newark, DE 19716 USA (phone: 302-831-8784;e-mail: weile@udel.edu).}
\thanks{Color versions of one or more of the figures in this communication are
available online at http://ieeexplore.ieee.org.}
\thanks{Digital Object Identifier 10.1109/\discretionary{}{}{}TAP.2016.xxx}}

\maketitle

\begin{abstract}
\boldmath
Despite the recent development in the implementation of time domain integral equation methods to the computation of electromagnetic scattering, the stability of those methods remain problematic. In most cases the stability of a method depends essentially on the reportage of the authors who first developed the method. Stability of time domain integral equation methods deeply tangled with the underlying integration used for computation of the kernel elements. Yet the nature of this relationship has not been fully researched. In this paper, we investigate the complicated relationship between the accuracy of integral computation of the kernel elements and the resulting stability of marching on in time. Numerical results demonstrate that stability can be improved by increasing integral accuracy.  And in many cases the near field basis integrals are more important than the far field. The stability goal can be achieved with more careful integration for different time domain implementations. Numerical results for a range of problems demonstrate these contentions.

\end{abstract}

\begin{IEEEkeywords}
Computational electromagnetics, integral equations, stability analysis, time domain scattering

\end{IEEEkeywords}

\section{Introduction}
\label{sec:introduction}

Recently we have seen an increasing interest in time domain integral equation (TDIT) based solvers throughout computational electromagnetic community. Indeed, they have been used to analysis a range of problems from wide band antenna analysis and radar cross section (RCS) to time varying and non-linear electromagnetic scattering and radiation. It has been shown that TDIE methods have certain advantages that are hard to find in any other method. As integral equation methods, they only require surface discretization of homogenous or impenetrable scatterers meanwhile they impose the radiation condition automatically. As time domain methods, they return broadband result in one simulation and are able to deal with time-varying and non linear problems.
 
Due to these intrinsic advantages and the increasing application need, over last two decades at least no less than five TDIE solution methods have been proposed that are stable for broad classes of problems: exact spatial integral method\cite{haduong}, the band limited interpolation function (BLIF) method~\cite{weile}, the convolution quadrature (CQ)~\cite{wang}, the marching on in degree method~\cite{jung}, and the spatial basis spanning series expansion~\cite{pray}. 

Although a lot success has been achieved, the TDIE based methods are in general haunted and hampered by some long existed issues such as instability. And due to the labyrinthine nature in the numerical simulation of electromagnetic phenomena it is quite complicated to precisely locate the causes of the instability. Among the common causes which lead to instability, despite its obvious potential effect, the accuracy of numerical integration has never been fully investigated yet. 

One important factor directly contributes to the inaccuracy of numerical integration is the so called ``shadow region'' issue. For all TDIE based solvers, a sharp transition between the region of a patch illuminated by the field radiated by another patch and the dark shadow region of the patch poses a real challenge. It further complicates the computations of time domain kernel elements, which are required in all the five methods. 

Among those five methods, the exact spatial integration method is based on finding these shadow areas exactly for the computation of kernel elements on each patch, and its application is largely limited to flat patches. The rest four are formulated to avoid the shadow region issue and compute integrals numerically. The BLIF method uses a smooth band limited basis function to ease the integration. The CQ method uses a Laplace domain to $\mathcal{Z}$ domain transform to avoid a shadow region issue for kernel element computation. The marching on in degree method uses weighted Laguerre polynomials as full domain basis functions. The spatial basis spanning series expansion method expands a conventional causal temporal Galerkin basis function into a series of functions that span the patch spatially and thus avoid the shadow region term-by-term in each region integrated. It is worth pointing out that exact spatial integral method, BLIF method and the spatial basis spanning series expansion method are all temporal Galerkin based method.

To compute the kernel elements, numerical quadrature computations are usually deployed (even the the ``exact" spatial integration method needs to do some numerical quadrature computations). Although this nature suggests the accuracy of the kernel element computations is paramount to achieve a stable method, the pertained papers all these five methods rarely discuss it in depth if at all. The reason for that may be that researchers tend to consider the numerical integration methods in general are ``exact'' enough to avoid scrutiny, or they just simply raise the integration rule order expecting to improve the accuracy without regard to the nature of the integrand contemplated. 

Our earlier work in the relationship between integral accuracy and stability~\cite{uluer} suggested the stability of the method is not necessarily improved by simply increasing the integration rule order. In this paper, we investigate the complicated relationship between numerical integration accuracy and the stability of TDIEs. All the simulations were done in the context of the CQ approach of~\cite{wang} and BLIF approach of~\cite{weile} for dielectrics. We will demonstrate that for dielectrics, increasing the accuracy of the numerical integrals involved for kernel element computations can improve the stability of these methods, and that in many cases integral accuracy in the near field plays a more important role than far field in governing their stability. This paper, complemented by our earlier works on perfect electrical conductors, will suggest that the five approaches currently available in the literature can achieve their stability goals in many cases by making possible integrals accurate enough because stability hinges critically on the implementation of the integrals computed. It also completes the literature about the stability of different methods in the current estimation of the community, where the stability depends essentially on the reportage of the authors who first developed the method.



\section{Formulation}
This section describes the implementation of the numerical algorithms used in this paper. Subsection~\ref{sec:NumSolns} discusses the derivation of time domain electric field and magnetic field integral equations (TD-EFIE and TD-MFIE), and briefly reviews the CQ and BLIF methods for TDIE discretization. Subsection~\ref{sec:AIM} describes the the adaptive integral method (AIM), which is used to analyze moderately large problems to expand our analysis beyond the limit of small problems. Finally, Subsection~\ref{sec:AdaptQuad} discusses the adaptive quadrature technique used in this paper so that certain accuracy control with some precision can be set in numerical integrations.

\subsection{Numerical Solution of Time Domain Integral Equations}~\label{sec:NumSolns}

\subsubsection{Formulation of time domain integral equations}
\begin{figure}[!t]
\centering
\includegraphics[width=2.5in]{scattering.pdf}
\caption{A classic scattering problem.}
\label{fig:scatter}
\end{figure}
A homogenous, penetrable object $S$ is illuminated by an incident electromagnetic wave with electric field $\mathbf{E}^{\mathrm{inc}}(\mathbf{r},t)$ and magnetic field $\mathbf{H}^{\mathrm{inc}}(\mathbf{r},t)$ as shown in Fig.~\ref{fig:scatter}. Region 1 is free space with permittivity $\mu_0$ and permeability $\epsilon_0$. Region 2 is characterized by relative permittivity $\mu_{\mathbf r}$ and relative permeability $\epsilon_{\mathbf r}$. 

 According to the surface equivalent principle, we can define equivalent current sources $\mathbf{J}(\mathbf{r},t)$ and magnetic currents $\mathbf{M}(\mathbf{r},t)$ on $S$ which replicate scattered electric fields $\mathbf{E}^{\mathrm{sca}}(\mathbf{r},t)$ and  $\mathbf{H}^{\mathrm{sca}}(\mathbf{r},t)$ in region 1, and internal electric fields $\mathbf{E}^{\mathrm{sca}}(\mathbf{r},t)$ and $\mathbf{H}^{\mathrm{sca}}(\mathbf{r},t)$ in region 2. 
For the purpose of convenience, we define an operator $l$ as:
\begin{equation}
\mathcal{L}_{c}(\mathbf f(\mathbf r, t)) = \iint\limits_S\frac{\mathbf f\left(\mathbf r^\prime, t-\frac{\left|\mathbf{r-r^\prime}\right|}{c}\right)}{4\pi \mathbf{\left|r-r^\prime\right|}}d\mathbf r^\prime
\label{eq:int}
\end{equation}
By combining the electrical field boundary condition and forcing the continuity of the electrical field, we derive a time domain integral equation for the electrical field of the form
\begin{multline}
	\dot{\mathbf E}^{\mathrm{inc}}(\mathbf r, t)=\mu_0\mathcal{L}_{c_0}(\ddot{\mathbf J}) - \frac{1}{\epsilon_0}\nabla\mathcal{L}_{c_0}
	(\nabla^\prime\cdot\mathbf J) + \nabla\times\mathcal{L}_{c_0}(\dot{\mathbf M}) \\ +\mu_r\mu_0\mathcal{L}_{c_r}(\ddot{\mathbf J}) - \frac{1}
	{\epsilon_r\epsilon_0}\nabla\mathcal{L}_{c_r}(\nabla^\prime\cdot\mathbf J) + \nabla\times\mathcal{L}_{c_r}(\dot{\mathbf M}).
\label{eq:tdie1}
\end{multline}
Similarly, by combining the magnetic field boundary condition, we write the time domain integral equation for the magnetic field as
\begin{multline}
	\dot{\mathbf H}^{\mathrm{inc}}(\mathbf r, t) =\epsilon_0\mathcal{L}_{c_0}(\ddot{\mathbf M}) - \frac{1}{\mu_0}\nabla\mathcal{L}_{c_0}
	(\nabla^\prime\cdot\mathbf M) - \nabla\times\mathcal{L}_{c_0}(\dot{\mathbf J}) \\ +\epsilon_r\epsilon_0\mathcal{L}_{c_r}(\ddot{\mathbf M}) - 
	\frac{1}{\mu_r\mu_0}\nabla\mathcal{L}_{c_r}(\nabla^\prime\cdot\mathbf M) - \nabla\times\mathcal{L}_{c_r}(\dot{\mathbf J}).
\label{eq:tdie2}
\end{multline}

\subsubsection{Convolution Quadrature Discretization}
Both Equation~\ref{eq:tdie1} and Equation~\ref{eq:tdie2} can be discretized by convolution quadrature(CQ). It is based on the continuous time Laplace transform and the discrete time $\mathcal{Z}$-transform. It begins by computing the Laplace transform the TDIEs. The resulting Laplace domain integral equations can be discretized in space using the usual Petrov-Galerkin approach. For our numerical examples, both basis and testing functions are those colloquially called GWP bases as described by  R. Graglia, D. Wilton, and A. Peterson~\cite{graglia}. 

Since that the temporal differentiation in the continuous time domain corresponds to multiplication by the Laplace parameter $s$ in the Laplace domain, and its backward finite difference approximation in discretized time domain is tantamount to multiplication by a function of the $\mathcal{Z}$-domain parameter $z$ in the $\mathcal{Z}$ domain, we replace $s$ with a function of $z$~\cite{wang}. Then by taking the inverse $\mathcal{Z}$-transform, we finally obtain a set of equations for the currents in the discrete time domain. The two backward finite difference approximations used here are the first-order backward difference (known as backward Euler or BE) and the second-order backward difference (known as BDF2) as described in~\cite{wang}.

Assuming a time step of size $\Delta t$, and denoting the current at time $i\Delta t$ on spatial basis function $n$ by $I_{ni}$ and the set of all $N_\mathrm{s}$ currents at time step $i$ by $\mathbf{I}_i$ we can write the equation for the currents in the discrete time domain as:
\begin{equation}
\mathbf{Z}_0\mathbf{I}_i=\mathbf{V}_i-\sum\limits_{j=0}^{i-1}\mathbf{Z}_{i-j}\mathbf{I}_j,
\label{eq:mot}
\end{equation}
which can be solved iteratively for the currents by marching on in time(MoT). 

\subsubsection{Bandlimited Interpolation Function Solution}
The BLIF method is a Petrov-Galerkin method, which means it uses temporal basis functions for the temporal discretization. It is a fundamentally different method from CQ. For temporal basis functions, we use the BLIFs, which are formulated in terms of the approximate prolate spheriodal wave functions (APSWFs) of Knab~\cite{knab}. They can interpolate a function bandlimited to an angular frequency $\omega_0$, and sampled with a time step less than the Nyquist step $\pi/\omega_0$.

Defining the time step $\Delta t$ in terms of an oversampling rate $\psi>1$ through $\Delta t=\pi/\psi\omega_0$ , the BLIFs have the form
\begin{equation}
T(t) = \frac {\sin(\psi\omega_0 t)}{\psi\omega_0 t}\frac{\sin[a\sqrt{(\frac{t}{N\Delta t})^2-1}]}{\sinh(a)\sqrt{({\frac{t}{N\Delta t}})^2-1}},
\end{equation}
where $N$ is the \emph{APSWF (half-)width parameter}, so-called because the function has approximate temporal support over an interval of length $(N+0.5)\Delta t$ into both the past and future, and $a = \pi N\Delta t$ is called the \emph{time-bandwidth} product of the APSWF. As explained in \cite{weile}, these functions are chosen in short because they are very smooth and provide an efficient, interpolatory representation of bandlimited functions. 

Despite all these compelling reasons for using BLIF as temporal basis functions, the difficulty with BLIFs is they cannot meet the causality criterion in the MoT algorithm.
since their future support destroys the causality which ensures the marching-on-in-time (MoT) algorithm works properly.  Therefore an extrapolation technique must be used to predict $N$ future current values based on $N_\mathrm{past}$ present and past currents~\cite{weile}. With this extrapolation scheme the capability to march on in time is recovered.

\subsection{Adaptive Integral Method}\label{sec:AIM}
To avoid bias, electrically large problems also should be taken into consideration. Therefore we need to use the adaptive integral method (AIM) to accelerate the solution of integral equations for electrically large problems. AIM was proposed first in frequency domain about three decades ago~\cite{bleszynski}, and was later extended to the time domain~\cite{ali}. Its key idea of AIM is to use the fast Fourier transform (FFT) to accelerate computation of fields through convolution with a Green's function. To enable the use of FFT, a set of point-like auxiliary basis functions located on uniformly spaced Cartesian grid nodes need to be created to approximate the continuous basis functions. This approximation is always inaccurate in the near field, so near field interactions are corrected to the exact value in any AIM implementation. The discrete time convolution in Equation~\ref{eq:mot} can also be accelerated by FFT if $Z$ matrices are grouped into blocks as shown in~\cite{ali}.

Note that in the AIM configuration, the far field integration is not done according to traditional numerical quadrature rules. To ensure that our conclusions are not affected by the AIM approximation, we extend the region of near field AIM corrections to be large enough that some integrals not subject to the AIM approximation are computed with every convergence criterion. 

\subsection{Adaptive Quadrature}\label{sec:AdaptQuad}
Since our primary concern in this paper is the effect of the accuracy of numerical integral in Equation~(\ref{eq:tdie1}) and Equation~(\ref{eq:tdie2}) on the stability of the algorithm, we use adaptive quadrature technique to bring some accuracy control of the integral.

By setting a relative error goal, adaptive quadrature calculates the given integral as a sum over smaller and smaller intervals until the desired error is achieved~\cite{press}.  For example, assume a triangle $S$ defined by vertices $A$, $B$ and $O$, and divided into four small triangles $S_1$, $S_2$, $S_3$ and $S_4$, by the mid-point of each edge, as shown in Fig.~(\ref{fig:triangle division}). Assume a numerical approximation $Q\!\left[S\right]$ to the surface integral of $f(x, y)$ over the triangle $S$ is computed first, followed by the approximation over the four sub triangles.  An error estimate $\epsilon$ is~\cite{press}:
\begin{IEEEeqnarray}{rCl}
\epsilon &=&\frac{\left| \sum\limits_{i=1}^{4} Q\!\left[S_i\right]-Q\!\left[S\right]\right|}{\left| \sum\limits_{i=1}^{4} Q\!\left[S_i\right]\right|}\label{eq:jielin}
\end{IEEEeqnarray}
If the estimated error is larger than the prescribed tolerance $\epsilon$, the triangles are equally subdivided again, and the process is repeated on each of them. The subdivision continues until the error is less than $\epsilon$ on every interval.
\begin{figure}[!t]
\centering
\includegraphics[width=2.5in]{Triangle.pdf}
\caption{A triangle division scheme. }
\label{fig:triangle division}
\end{figure}
Adaptive quadrature can bring some accuracy control by setting the error bound. It is also not effected by the dimension (or shape) of the integration domain. Besides, it is relative independent from the actual numerical quadrature rules underneath $Q\!\left[S\right]$.

\section{Numerical Result}
In this section, numerical results are presented to demonstrate the impact of integral accuracy on the stability of the TDIE implementations. All scatters simulated here are dielectrics. and both EFIE and MFIE are used.

All the scatter are illuminated by a Gaussian pulse given by
\begin{equation}
\mathbf{E}^{\mathrm{inc}}\left(z, t\right)=\hat{\mathbf{x}}\exp\!\!\left[\frac{1}{2\sigma^2}\left(t-\frac{z}{c}-\tau\right)^2\right]\!\cos\!\!\left[2\pi f_0\left(t-\frac{z}{c}\right)\right].
\end{equation}
where $f_0$ is a center frequency of the incident wave, and $\tau$ is a delay. The temporal standard deviation $\sigma$ is related to a nominal bandwidth $f_\mathrm{bw}$ by $\sigma=6/\left(2\pi f_\mathrm{bw}\right)$.

For all the scatters, two kinds of radar cross-section (RCS) are computed for different frequencies and angles as the results presented here. Bistatic RCS results for different frequencies were computed from 181 elevation angles between $\theta = 0^\circ$ and $\theta = 180^\circ$. The azimuthal angle was set to $\phi = 0^\circ$.  Monstatic RCS results were computed for different frequencies. 

For the integral where a basis patch and a test patch overlap, the self integration rules described in~\cite{weiletoo} are used

For self integration where a basis patch and a test patch overlap, the integration rules described in~\cite{weiletoo} are used. For the non-self integration, we define a distance $d_\mathrm{near}$ such that basis integrations are called \emph{near} when their centroids are separated by less than this distance and \emph{far} otherwise. One of two adaptive rules with error depending on their proximity is applied. In all cases presented here that do not use AIM, we set this distance to $0.4\times \lambda_\mathrm{min}$, where $\lambda_\mathrm{min}$ corresponds to the shortest wavelength of interest in the simulation. The basic integration rules underlying the adaptive scheme are low order Dunavant rules described in~\cite{dunavant}. Testing integration is always accomplished with a non-adaptive fifth-order Dunavant rule. A simulation is called ``stable'' if all currents were less than 0.001 A/m$^2$ at the end of the simulation. Numerical results were obtained for different values of the error allowed by the adaptive routine. A table is used to indicate stability of different runs, where shadow squares mark unstable computations and white squares mark stable ones. 

\subsection{Ogive}
The first example examined is a dielectric ogive with relative permittivity $\epsilon_r$ = 4.0 and relative permeability $\mu_r$ = 1.0, as shown in Fig.~\ref{ogive}. The ogive axis is about 2m long and its largest diameter is 0.4cm. It is meshed with 304 flat patches. We set total time steps $N_\mathrm{t}$ to 4000 and the length of each time step $\Delta t$ to 100ps. For the incident wave, the nominal frequency band $f_{bw}$ was chosen to be 300MHz, the central frequency $f_0$ was 100MHz, and the delay $\tau$ was 20$\mu$s. RCS results at 21 equally-spaced frequencies from 200MHz to 240MHz were computed. The spatial basis functions are the zeroth-order GWP bases commonly known as Rao-Wilton-Glisson, or RWG, bases~\cite{RWG}. For the BLIF method, the number of the past steps used for extrapolation was $N_\mathrm{samp}=6$, the APSWF width was $N=7$, and the APSWF bandwidth was $\omega_0=2\pi\times160$MHz. The testing rule was set to fifth-order, and the adaptive rules were set to first-order.

Figs.~\ref{DielOgiveStabilityCQ} show the stability pattern for the CQ method. These figures show that once the relative error of near field basis integral is lower than 80$\%$, the accuracy requirement for the far field can be almost ignored with proper near-far division.
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{DielOgiveStabilityCQ.pdf}}
\caption{Stability of the simulation versus integration accuracy for a dielectric ogive for CQ.}
\label{DielOgiveStabilityCQ}
\end{figure}
Fig.~\ref{DielOgiveStabilityBLIF} shows the stability pattern for BLIF method. BLIF has a better stability pattern for this dielectric ogive compared with the conducting ogive of the last chapter that may be due to the much smaller time step length. It also indicates that the far field integral accuracy is less important than the near field accuracy, as expected.
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{DielOgiveStabilityBLIF.pdf}}
\caption{Stability of the simulation versus integration accuracy for a dielectric ogive for BLIF.}
\label{DielOgiveStabilityBLIF}
\end{figure}

Fig.~\ref{dielogive} shows the comparison of the RCS results obtained for this problem by the two time domain methods and the frequency domain method. The time domain RCS data for these figures was obtained with the relative error tolerance in the adaptive quadrature to 0.2 for both near field and far field integral to ensure accurate results. An excellent correspondence can be observed among these three curves.
\begin{figure}[h]
\subfloat[RCS at 200MHz]{\includegraphics[width = 0.5\columnwidth]{DielOgive200MHz.jpg}} 
\subfloat[RCS at 220MHz]{\includegraphics[width = 0.5\columnwidth]{DielOgive220MHz.jpg}}\\
\subfloat[RCS at 240MHz]{\includegraphics[width = 0.5\columnwidth]{DielOgive240MHz.jpg}}
\subfloat[RCS vs. Frequency]{\includegraphics[width = 0.5\columnwidth]{DielOgiveRCSvsFreq.jpg}} 
\caption{Comparison of RCS results for a dielectric ogive. The bistatic RCS results at start point, middle point and the end point of the frequency range are shown in (a), (b) and (c). The monostatic RCS vs. frequency at elevation angle $\theta=0$ is shown in (d).}
\label{dielogive}
\end{figure}

\subsection{Slab}
Next we simulate electromagnetic scattering from a large slab shown in Fig.~\ref{tank}. The length and the width of the prism are 3m. The height of the prism is 0.8m. The relative permittivity $\epsilon_r$ is 4.0 and the relative permeability $\mu_r$ is 1.0. It is meshed with 1684 patches supporting a total of 5052 0th-order GWP bases. To accelerate the computation, AIM is used for this example. The incident wave for both examples is the same, with nominal frequency band $f_\mathrm{bw}=40$MHz, central frequency $f_0=120$MHz, and incident time delay 0.2$\mu$s. The time step length $\Delta t$ for CQ was chosen as 300 ps and BLIF was chosen as 200 ps. Because of the need for extrapolation, the BLIF method is limited to small time steps and accurate results. The number of total time steps was $N_\mathrm{t}$=1200 for CQ and $N_\mathrm{t}$=1800 for BLIF. The near field integration threshold is $d=0.6$m. The near field integration threshold is $d=0.4$m. The testing rule was set to fifth-order, and the adaptive rules were set to third-order. The AIM grid size is 0.1m. 
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{slab.png}}
\caption{A perfectly conducting scatterer approximating a tank.}
\label{tank}
\end{figure}

Fig.~\ref{slabrcs} shows the comparison of the RCS results obtained by the three different methods. RCS results were obtained at 21 frequencies located from 100MHz to 140MHz with equal intervals. In the time domain codes, we set the relative error tolerance in the adaptive quadrature to 0.3 for both near field and far field integrals to generate these particular figures.
\begin{figure}[h]
\subfloat[RCS at 100MHz]{\includegraphics[width = 0.5\columnwidth]{Slab100MHz.jpg}} 
\subfloat[RCS at 120MHz]{\includegraphics[width = 0.5\columnwidth]{Slab120MHz.jpg}}\\
\subfloat[RCS at 140MHz]{\includegraphics[width = 0.5\columnwidth]{Slab140MHz.jpg}}
\subfloat[RCS vs. Frequency]{\includegraphics[width = 0.5\columnwidth]{SlabRCSvsFreq.jpg}} 
\caption{Bistatic RCS of the slab computed at (a) 100MHz, (b) 120MHz, and (c) 140MHz, and (d) the monostatic RCS between 100 and 140 MHz.}
\label{slabrcs}
\end{figure}
 The results from the BLIF method show better correspondence seen in Fig.~\ref{slabrcs}(d). The RCS results from CQ have a greater discrepancy with the frequency domain results, but this is to be expected from this less accurate method. Also note that AIM relies crucially on an approximation that is actually out of accuracy control brought by adaptive quadrature technique. Hence there is no guarantee that better accuracy in the computation of basis function integrations in general will preserve the behavior of the method.
 
 Fig.~(\ref{BDF2SlabStability}) shows the stability pattern for CQ method. 
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=0.75\columnwidth]{SlabStabilityCQ.pdf}}
\caption{Stability of the simulation versus integration accuracy for a dielectric slab for CQ.}
\label{BDF2SlabStability}
\end{figure}
Fig.~(\ref{BLIFSlabStability}) shows the stability pattern for BLIF method.
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=0.75\columnwidth]{SlabStabilityBLIF.pdf}}
\caption{Stability of the simulation versus integration accuracy for a dielectric slab for BLIF.}
\label{BLIFSlabStability}
\end{figure}
This pattern also shows the relative insignificant role the far field integration has on accuracy and stability. Despite the difficulty posed by AIM to accuracy control, the results confirm that a more strict integral accuracy eventually ensures stability.

\section{Dielectric Sphere}
Due to the fact that the ogive and the slab are meshed with flat patches, the next example is a sphere meshed by perfect spherical patches. The sphere is of 1.0m radius. The sphere is a dielectric with $\epsilon = 2.0$ and $\mu = 1.0$ It was meshed into 128 perfect spherical triangular patches. The incident wave is of 40MHz nominal frequency band, 120MHz center frequency, and time delay to the origin of 0.2$\mu$s. The choice of the spatial basis was also different from the previous cases. We used first-order basis functions instead of zeroth-order basis functions to increase the difficulty of maintaining stability. The near field integration threshold was set to 0.86m. Testing integration is accomplished with a non-adaptive fifth-order rule. The adaptive rules were built on one-point integrations. The temporal discretization is consist of total 800 time steps of 250ps each for both CQ and BLIF. The BLIF method parameters were $N_\mathrm{past}=5$, $N=6$, and $f_0=200$MHz. 

Fig.~\ref{dielsphrcs} shows the RCS results at 21 frequencies located from 100 MHz to 140 MHz with 2MHz increment. The relative error tolerance in the adaptive quadrature was 0.2 for both near field and far field integral. The figure compares our time domain simulation results to the Mie series result, since analytical results can be computed for spheres. Due to the long time step size, the discrepancy tends to be larger when the frequency goes higher for both methods.
\begin{figure}[h]
\subfloat[RCS at 100MHz]{\includegraphics[width = 0.5\columnwidth]{DielSphere100MHz.jpg}} 
\subfloat[RCS at 120MHz]{\includegraphics[width = 0.5\columnwidth]{DielSphere120MHz.jpg}}\\
\subfloat[RCS at 140MHz]{\includegraphics[width = 0.5\columnwidth]{DielSphere140MHz.jpg}}
\subfloat[RCS vs. Frequency]{\includegraphics[width = 0.5\columnwidth]{DielSphereRCSvsFreq.jpg}} 
\caption{Comparison of RCS results of a dielectric sphere. The bistatic RCS results at start point, middle point and the end point of the frequency range are shown in (a), (b) and (c). The monostatic RCS vs. frequency at elevation angle $\theta=0$ is shown in (d).}
\label{dielsphrcs}
\end{figure}

Fig.~\ref{BDF2DielSphereStability} shows the stability pattern for the CQ method. 
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{BDF2DielSphereStability.pdf}}
\caption{Stability of the simulation versus integration accuracy for a homogeneous dielectric sphere for CQ.}
\label{BDF2DielSphereStability}
\end{figure}
Fig.~\ref{BLIFDielSphereStability} shows the stability results for the BLIF method. We take a more strict standard to make sure all the RCS data from the ``stable'' simulations has no more than $20\%$ relative error compared with the Mie series result. The result from CQ shows that the relative importance of near field integral accuracy to accomplish stability. The results from BLIF show the difficulty in achieving stability in dielectric computations. Yet with proper accuracy control, it can be rendered stable, even given the much larger time step length compared with the case of the slab.
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{BLIFDielSphereStability.pdf}}
\caption{Stability of the simulation versus integration accuracy for a homogeneous dielectric sphere for BLIF.}
\label{BLIFDielSphereStability}
\end{figure}

\section{Dielectric Sphere with Conductor Core}
The next example is a system of two concentric spheres. The inner sphere is of 0.5m radius. It was meshed into 72 perfect spherical triangular patches. The outer sphere is of 1.0m radius and was meshed into 128 perfect spherical triangular patches. The inner sphere is a perfect conductor and the outer sphere is a dielectric with $\epsilon = 4.0$ and $\mu = 1.0$. The incident wave is of 40MHz nominal frequency band, 120MHz center frequency, and time delay to the origin of 0.2$\mu$s. The spatial basis used first-order basis functions. The near field integration threshold was set to 0.86m. The testing rule was set to fifth-order, and the adaptive rules were built on one-point integrations. The temporal discretization is consist of total 1400 time steps of 250ps each. The BLIF method parameters were $N_\mathrm{past}=5$, $N=6$, and $f_0=200$MHz.

Fig.~\ref{condcoresphrcs} shows the RCS results at 41 frequencies located from 100 MHz to 140 MHz with 1 MHz increment. The relative error tolerance in the adaptive quadrature was 0.2 for both near field and far field integrals. The figure compares our time domain simulation results to the Mie series result. Both methods yielded very good results compared with the analytic solution. The BLIF method produced a more accurate result, especially in the low frequency range. Because the time step length is relatively long, the discrepancy in the high frequency range tends to be larger. 
\begin{figure}[h]
\subfloat[RCS at 100MHz]{\includegraphics[width = 0.5\columnwidth]{CondCore100MHz.jpg}} 
\subfloat[RCS at 120MHz]{\includegraphics[width = 0.5\columnwidth]{CondCore120MHz.jpg}}\\
\subfloat[RCS at 140MHz]{\includegraphics[width = 0.5\columnwidth]{CondCore140MHz.jpg}}
\subfloat[RCS vs. Frequency]{\includegraphics[width = 0.5\columnwidth]{CondCoreRCSvsFreq.jpg}} 
\caption{Comparison of RCS results of a dielectric sphere with a conductor core. The bistatic RCS results at start point, middle point and the end point of the frequency range are shown in (a), (b) and (c). The monostatic RCS vs. frequency at elevation angle $\theta=0$ is shown in (d).}
\label{condcoresphrcs}
\end{figure}

Fig.~\ref{BDF2CondCoreStability} shows the stability pattern for the CQ method. 
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{BDF2CondCoreSphereStability.pdf}}
\caption{Stability of the simulation versus integration accuracy for a dielectric sphere with a perfect conducting core for CQ.}
\label{BDF2CondCoreStability}
\end{figure}
Fig.~\ref{BLIFCondCoreStability} shows the stability results for the BLIF method. Compared with the last case, the stability  criterion here is stricter due to the more complicated interactions necessary to model this composite structure. Still, the results confirm that a more strict integral accuracy eventually ensures stability, and that the integration error tolerance needed for stability is not outrageously small.
\begin{figure}[h]
\centering
\centerline{\includegraphics[width=\columnwidth]{BLIFCondCoreSphereStability.pdf}}
\caption{Stability of the simulation versus integration accuracy for a dielectric sphere with a perfect conducting core for BLIF.}
\label{BLIFCondCoreStability}
\end{figure}

\section{Conclusion}

This paper investigate the complicated relationship between the actual numerical integral accuracy and the stability of TDIEs. Two fundamentally different TDIE solution methods, CQ and BLIF, were used to simulate different kinds of homogeneous dielectric objects. For large objects, AIM was used to accelerate the computations. Two adaptive quadrature rules with different error tolerances were applied to the nonsingular basis function integrations, to introduce proper accuracy control,. Under these conditions, stability can be achieved with integration rules in the cases where they previously failed~\cite{uluer}.

The numerical results of this experiment demonstrate that the stability of TDIE implementations may be deeply affected by the inaccuracy of naive spatial integral computations. The results show that the stability of TDIEs can be improved by higher integral accuracy, and that in many cases the effect is more profound in the near field than in the far field. Earlier related work ran the similar experiment with Gaussian integration rules of different orders indicates that increasing order does not necessarily improve the stability. That said, integral accuracy should not be conflated with integral order.  

Unlike computational complexity, the stability of TDIE solvers is never quantified. By demonstrating the key role that kernel element computation plays governing the stability of TDIEs, we essensially suggest a potential new perspective to achieve the stability goal by paying more attention to this seemingly trivial detail. We hope our work can lead to more discussion about this topic. 



\bibliography{LiPECBib}{}
\bibliographystyle{IEEEtran}

\end{document}
